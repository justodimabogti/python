"""
Source File Name   : spaceInvaders1.2py
Author's Name      : Justo Dimabogti
Last Modified by   : Justo Dimabogti
Date Last Modified : August 8, 2012

Program Description: A space invaders scrolling game.
       Version 03  : Added Class Shoot to use as weapons
       Version 02  : Added the Class UFo to generate addional enemies
       Version 01. : This version is based  from my previous Side Scroller game
                     Changed the scrolling from top to bottom

"""

# import pygame module    
import pygame, random

pygame.init()

screen = pygame.display.set_mode((640, 480))
class Shoot(pygame.sprite.Sprite):
     
    def __init__(self):
        # Call the parent class (Sprite) constructor
        pygame.sprite.Sprite.__init__(self) 
 
        self.image = pygame.image.load("shoot2.png").convert()
        self.rect = self.image.get_rect()

#control of avatar movement
class Ship(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        #load the ship image

        self.image = pygame.image.load("spaceship.gif")
        self.image= pygame.transform.scale(self.image,(100,70))
        self.image = self.image.convert()
        self.rect = self.image.get_rect()

        if not pygame.mixer:
            print("problem with sound")
        else:
            #loading audio sounds
            pygame.mixer.init()
            self.sndEat = pygame.mixer.Sound("yippy.wav")
            self.sndExplode = pygame.mixer.Sound("explode003.wav")
            self.sndWave = pygame.mixer.Sound("thrust.wav")
            self.sndWave.play(-1)
        
    def update(self):
        mousex, mousey = pygame.mouse.get_pos()
        self.rect.center = (mousex, 430)
        
#this Class Points where the Ship catch the the random fishes         
class Points(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        #loaded fishes images
        pointsImage1 = pygame.image.load("100pts.gif").convert()
        pointsImage2 = pygame.image.load("200pts.gif").convert()
        pointsImage3 = pygame.image.load("400pts.gif").convert()    
        pointsImage4 = pygame.image.load("400pts.gif").convert()    

        #fishes island
        self.pointsImages = (pointsImage1, pointsImage2,pointsImage3,pointsImage4)
        self.reset()

        self.dy = 10
    
    def update(self):
        self.rect.centery += self.dy
        if self.rect.top > screen.get_height():
            self.reset()
            
    def reset(self):
        pointsRandSze = random.randint(30, 50) 
        self.image = self.pointsImages[random.randint(0,2)]
        self.image = pygame.transform.scale(self.image, (pointsRandSze, pointsRandSze))
        self.rect = self.image.get_rect()
        self.rect.bottom = 0
        self.rect.centerx = random.randrange(0, screen.get_width())


#class Comets are the enemies
class Comets(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
       #loading the master image & convert the image         
        imgMaster = pygame.image.load("asteriodSpriteGroup.bmp")
        imgMaster = imgMaster.convert()
        #creating am empty list        
        self.imgList = []
        #the original size of the sub-images
        imgSize = (255,236)
        offset = ((10, 10), (250, 10), (10, 260), (250, 260))        
        for i in range(4):
            tmpImg = pygame.Surface(imgSize)
            tmpImg.blit(imgMaster, (0, 0), (offset[i], imgSize))
            transColor = tmpImg.get_at((1, 1))
            tmpImg.set_colorkey(transColor)
            
            self.imgList.append(tmpImg)
        self.reset()

    def update(self):
        self.rect.centerx += self.dx
        self.rect.centery += self.dy
        if self.rect.top > screen.get_height():
            self.reset()
    
    def reset(self):
        
        self.image = self.imgList[random.randint(0,3)]
        randSize = random.randint(20, 50)
        self.image = pygame.transform.scale(self.image, (randSize, randSize)) #resize the cloud, randSize is the size value for both X and Y
        self.rect = self.image.get_rect()
        self.rect.bottom = 0 
        self.rect.centerx = random.randrange(0, screen.get_width())
        self.dy = random.randrange(5, 10)
        self.dx = random.randrange(-2, 2)
        

#the background that moves in right direction    
class Space(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        #loading the background image
        self.image = pygame.image.load("space.png")
        self.rect = self.image.get_rect()
        self.dy = 2
        self.reset()
        
    def update(self):
        self.rect.bottom += self.dy
        if self.rect.bottom >= 1440:
            self.reset() 
    
    def reset(self):
        self.rect.top = -960


class UFO(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("ufoufo2.gif")
        self.image= pygame.transform.scale(self.image,(80,50))

        self.image = self.image.convert()
        self.rect = self.image.get_rect()
        self.reset()

    def update(self):
        self.rect.centerx += self.dx
        self.rect.centery += self.dy
        if self.rect.top > screen.get_height():
            self.reset()
    
    def reset(self):
        self.rect.bottom = 0
        self.rect.centerx = random.randrange(0, screen.get_width())
        self.dy = random.randrange(5, 10)
        self.dx = random.randrange(-2, 2)
    

#updating the scrore
class Scoreboard(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.lives = 5
        self.score = 0
        self.font = pygame.font.SysFont("Calibri", 25)
        
    def update(self):
        self.text = "Ship: %d, score: %d" % (self.lives, self.score)
        self.image = self.font.render(self.text, 1, (255, 255, 0))
        self.rect = self.image.get_rect()
        

# This is the game function - it houses the main game loop    
def game():
    pygame.display.set_caption("Space Invaders")

    background = pygame.Surface(screen.get_size())
    background.fill((0, 0, 255))
    screen.blit(background, (0, 0))
    ship = Ship()
    points  = Points()
    comets1 = Comets()
    comets2 = Comets()
    comets3 = Comets()
    ufo = UFO()
    shoot = Shoot()
    space = Space()
    scoreboard = Scoreboard()

    friendSprites = pygame.sprite.OrderedUpdates(space,points, ship)
    cometsprites = pygame.sprite.Group(ufo,comets1, comets2, comets3)    
    scoreSprite = pygame.sprite.Group(scoreboard)

    clock = pygame.time.Clock()
    keepGoing = True

    #added music
    gameMusic = pygame.mixer.Sound("MyMusic.ogg")
    gameMusic.play(-1)
    
    keepGoing = True
    while keepGoing:
        clock.tick(30)
        pygame.mouse.set_visible(False)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                keepGoing = False

        
        #check collisions
        
        if pygame.sprite.collide_mask(ship,points):
            ship.sndEat.play()
            points.reset()
            scoreboard.score += 100

        hitComets1 = pygame.sprite.collide_mask(ship, comets1)
        hitComets2 = pygame.sprite.collide_mask(ship, comets2)
        hitComets3 = pygame.sprite.collide_mask(ship, comets3)
        hitComets4 = pygame.sprite.collide_mask(ship, ufo)
        if hitComets1 or hitComets2 or hitComets3 or hitComets4:    
            ship.sndExplode.play()
            scoreboard.lives -= 1
            if scoreboard.lives <= 0:
                keepGoing = False
            if hitComets1:
                comets1.reset()
            if hitComets2:
                comets2.reset()
            if hitComets3:
                comets3.reset()
            if hitComets4:
                ufo.reset()

        #update sprites
        friendSprites.update()
        cometsprites.update()
        scoreSprite.update()

        #draw screen sprites
        friendSprites.draw(screen)
        cometsprites.draw(screen)
        scoreSprite.draw(screen)

        #flip the screen
        pygame.display.flip()

    #stop the wave sound
    ship.sndWave.stop()
    
    #return mouse cursor
    pygame.mouse.set_visible(True) 
    return scoreboard.score
    
def instructions(score):
    pygame.display.set_caption("Defending the Space!")

    ship= Ship()
    space = Space()
    
    allSprites = pygame.sprite.Group(space, ship)
    insFont = pygame.font.SysFont("Calibri", 20)
    insLabels = []
    instructions = (
    "Space Ship.     Last score: %d" % score ,
    "Instructions:  You are in a Space,",
    "avoiding the comets and UFO.",
    "",
    "Navigate your Ship and catches a some flying object",
    "to earn a score, but be careful with  comets and UFO",
    "coming your way and try your best to avoid it.",
    "Stir the Mouse...",
    "good luck!",
    "",
    "click to start, escape to quit..."
    )
    
    for line in instructions:
        tempLabel = insFont.render(line, 1, (255, 255,0))
        insLabels.append(tempLabel)
    
    keepGoing = True
    clock = pygame.time.Clock()
    pygame.mouse.set_visible(False)
    while keepGoing:
        clock.tick(30)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                keepGoing = False
                donePlaying = True
            if event.type == pygame.MOUSEBUTTONDOWN:
 
                keepGoing = False            
                donePlaying = False

            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    keepGoing = False
                    donePlaying = True

            
        allSprites.update()
        allSprites.draw(screen)

        for i in range(len(insLabels)):
            screen.blit(insLabels[i], (50, 30*i))

        pygame.display.flip()
        
    ship.sndWave.stop()    
    pygame.mouse.set_visible(True)
    return donePlaying

#This is the main function        
def main():
    donePlaying = False
    score = 0
    while not donePlaying:
        donePlaying = instructions(score)
        if not donePlaying:
            score = game()


if __name__ == "__main__":
    main()
    
